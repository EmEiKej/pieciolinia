using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    public E_SoundName sound;
    public int tone;
    public void Determine_Sound_Tone(E_SoundName sound, int tone) 
    {
        this.sound = sound;
        this.tone = tone;
    }
}
