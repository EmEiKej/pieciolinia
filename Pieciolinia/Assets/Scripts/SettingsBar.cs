using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SettingsBar : MonoBehaviour
{
    [SerializeField] GameObject playSettings;
    [SerializeField] GameObject file;
    [SerializeField] GameObject newProject;
    [SerializeField] GameObject openProject;
    [SerializeField] GameObject tools;
    [SerializeField] GameObject piano;
    [SerializeField] GameObject noteToolbox;
    [SerializeField] GameObject save;
    [SerializeField] GameObject savetype;
    [SerializeField] GameObject pieciolinia;
    [SerializeField] GameObject leftHandTact;
    [SerializeField] GameObject rightHandTact;
    [SerializeField] ProjectContoller projectContoller;
    List<Tact> leftHandTrack = new List<Tact>();
    List<Tact> rightHandTrack = new List<Tact>();
    string playType = "BothHands";
    List<int> pauseIndex = new List<int> { 0, 1 };
    private void Start()
    {
        leftHandTrack = pieciolinia.GetComponent<Pieciolinia>().leftHandTrack;
        rightHandTrack = pieciolinia.GetComponent<Pieciolinia>().rightHandTrack;
    }
    public void OnSettingsBarButtonExitClicked()
    {
        save.SetActive(true);
    }
    public void OnSettingsBarButtonPlaySettingsClicked()
    {
        playSettings.SetActive(!playSettings.activeSelf);
    }
    public void OnPlaySettingsButtonLeftHandClicked()
    {
        playSettings.SetActive(false);
        playType = "LeftHand";
    }
    public void OnPlaySettingsButtonRightHandClicked()
    {
        playSettings.SetActive(false);
        playType = "RightHand";
    }
    public void OnPlaySettingsButtonBothHandsClicked()
    {
        playSettings.SetActive(false);
        playType = "BothHands";
    }
    public void OnSettingsBarButtonToolsClicked()
    {
        tools.SetActive(!tools.activeSelf);
    }
    public void OnSettingBarButtonAddTactClicked()
    {
        pieciolinia.GetComponent<Pieciolinia>().AddTact();
    }
    public void OnSettingsBarButtonRemoveTactClicked()
    {
        foreach (Sound sound in leftHandTrack.Last().sounds)
        {
            Destroy(sound.gameObject);
        }
        Destroy(leftHandTrack.Last().gameObject);
        leftHandTrack.RemoveAt(leftHandTrack.Count - 1);
        foreach (Sound sound in rightHandTrack.Last().sounds)
        {
            Destroy(sound.gameObject);
        }
        Destroy(rightHandTrack.Last().gameObject);
        rightHandTrack.RemoveAt(rightHandTrack.Count - 1);
        projectContoller.tacts--;
        piano.GetComponent<Piano>().RemoveLastTactDropDown();
    }
    public void OnSettingsBarButtonPlayClicked()
    {
        for (int i = 0; i < leftHandTrack.Count; i++)
        {
            leftHandTrack[i].sounds.Sort((p1, p2) => p1.position.CompareTo(p2.position));
            rightHandTrack[i].sounds.Sort((p1, p2) => p1.position.CompareTo(p2.position));
        }
        if (playType == "BothHands")
        {
            StartCoroutine(PlayLeftHand());
            StartCoroutine(PlayRightHand());
        }
        else if (playType == "LeftHand")
        {
            StartCoroutine(PlayLeftHand());
        }
        else if (playType == "RightHand")
        {
            StartCoroutine(PlayRightHand());
        }
    }
    IEnumerator PlayLeftHand()
    {
        for (int i = 0; i < leftHandTrack.Count; i++)
        {
            for (int j = 0; j < leftHandTrack[i].sounds.Count; j++)
            {
                leftHandTrack[i].sounds[j].gameObject.transform.GetChild(0).GetComponent<AudioSource>().Play();
                leftHandTrack[i].sounds[j].myRenderer.color = Color.cyan;
                yield return new WaitForSeconds(leftHandTrack[i].sounds[j].note);
                leftHandTrack[i].sounds[j].gameObject.transform.GetChild(0).GetComponent<AudioSource>().Stop();
                leftHandTrack[i].sounds[j].myRenderer.color = Color.black;
            }
        }
    }
    IEnumerator PlayRightHand()
    {
        for (int i = 0; i < rightHandTrack.Count; i++)
        {
            for (int j = 0; j < rightHandTrack[i].sounds.Count; j++)
            {
                rightHandTrack[i].sounds[j].gameObject.transform.GetChild(0).GetComponent<AudioSource>().Play();
                rightHandTrack[i].sounds[j].myRenderer.color = Color.cyan;
                yield return new WaitForSeconds(rightHandTrack[i].sounds[j].note);
                rightHandTrack[i].sounds[j].gameObject.transform.GetChild(0).GetComponent<AudioSource>().Stop();
                rightHandTrack[i].sounds[j].myRenderer.color = Color.black;
            }
        }
    }
    public void OnSettingsBarButtonStopClicked()
    {
        StopAllCoroutines();
    }
    public void OnToolsButtonsPianoClicked()
    {
        tools.SetActive(false);
        piano.SetActive(true);
    }
    public void OnToolsButtonsNoteToolboxClicked()
    {
        tools.SetActive(false);
        noteToolbox.SetActive(true);
    }
}
