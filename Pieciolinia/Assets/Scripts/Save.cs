using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using TMPro;
using static UnityEngine.UI.Image;
using UnityEngine.UIElements;

public class Save : MonoBehaviour
{
    [SerializeField] Pieciolinia pieciolinia;
    [SerializeField] GameObject saveType;
    [SerializeField] TMP_InputField path;
    [SerializeField] ProjectContoller projectContoller;
    AudioClip clipLeft;
    AudioClip clipRight;
    AudioClip audioClip;
    public void OnButton_Exit_Clicked()
    {
        gameObject.SetActive(true);
    }
    public void OnButton_No_Clicked()
    {
        Application.Quit();
    }
    public void OnButton_Yes_Clicked()
    {
        saveType.SetActive(true);
        path.text = projectContoller.filePath + "\\" + projectContoller.projectName + "." + "wav";
        gameObject.SetActive(false);
    }
    public void On_Save_Button_Exit_Clicked()
    {
        gameObject.SetActive(false);
    }
    public void On_SaveType_Button_Exit_Clicked()
    {
        gameObject.SetActive(true);
        saveType.SetActive(false);
    }
    public void OnButton_Save_Clicked()
    {
        clipLeft = CombineAudioClips(CutAudio(pieciolinia.leftHandTrack));
        clipRight = CombineAudioClips(CutAudio(pieciolinia.rightHandTrack));
        audioClip = CombainHands(clipLeft, clipRight);
        WriteWav(audioClip);
        Application.Quit();
    }
    List<AudioClip> CutAudio(List<Tact> track)
    {
        List<AudioClip> cutAudio = new List<AudioClip>();
        foreach (Tact tact in track)
        {
            foreach (Sound sound in tact.sounds)
            {
                int length = Mathf.FloorToInt(sound.gameObject.transform.GetChild(0).GetComponent<AudioSource>().clip.frequency * sound.note);
                float[] cutSamples = new float[length];
                sound.gameObject.transform.GetChild(0).GetComponent<AudioSource>().clip.GetData(cutSamples, 0);
                cutAudio.Add(AudioClip.Create("CutAudio", length, sound.gameObject.transform.GetChild(0).GetComponent<AudioSource>().clip.channels, sound.gameObject.transform.GetChild(0).GetComponent<AudioSource>().clip.frequency, false));
                cutAudio.Last().SetData(cutSamples, 0);
            }
        }
        return cutAudio;
    }
    static AudioClip CombineAudioClips(List<AudioClip> audioClips)
    {
        int length = 0;

        foreach (var clip in audioClips)
        {
            length += clip.samples;
        }
        float[] combinedData = new float[length];

        int currentPosition = 0;
        foreach (var clip in audioClips)
        {
            float[] data = new float[clip.samples];
            clip.GetData(data, 0);
            data.CopyTo(combinedData, currentPosition);
            currentPosition += data.Length;
        }
        AudioClip firstClip = audioClips[0];
        AudioClip combinedClip = AudioClip.Create("CombinedClip", length, firstClip.channels, firstClip.frequency, false);
        combinedClip.SetData(combinedData, 0);
        return combinedClip;
    }
    static AudioClip CombainHands(AudioClip leftHand, AudioClip rightHand)
    {
        int length = 0;
        length += leftHand.samples;
        length += rightHand.samples;
        float[] dataLeft = new float[leftHand.samples];
        leftHand.GetData(dataLeft, 0);
        float[] dataRight = new float[rightHand.samples];
        rightHand.GetData(dataRight, 0);
        float[] combinedData = new float[length];
        int currentPosition = 0;
        if (dataLeft.Length > dataRight.Length)
        {
            for (int i = 0; i < dataRight.Length; i++) 
            {
                combinedData[currentPosition] = dataRight[i];
                currentPosition++;
                combinedData[currentPosition] = dataLeft[i];
                currentPosition++;
            }
            for (int i = dataRight.Length; i < dataLeft.Length; i++) 
            {
                combinedData[currentPosition] = dataLeft[i];
                currentPosition++;
            }
        }
        else 
        {
            for (int i = 0; i < dataLeft.Length; i++)
            {
                combinedData[currentPosition] = dataLeft[i];
                currentPosition++;
                combinedData[currentPosition] = dataRight[i];
                currentPosition++;
            }
            for (int i = dataLeft.Length; i < dataRight.Length; i++)
            {
                combinedData[currentPosition] = dataRight[i];
                currentPosition++;
            }
        }
        AudioClip firstClip = leftHand;
        AudioClip combinedClip = AudioClip.Create("CombinedClip", length, firstClip.channels, firstClip.frequency, false);
        combinedClip.SetData(combinedData, 0);
        return combinedClip;
    }
    void WriteWav(AudioClip clip)
    {
        var data = new float[clip.samples * clip.channels];
        clip.GetData(data, 0);
        string path = this.path.text;
        using (var stream = new FileStream(path, FileMode.CreateNew, FileAccess.Write))
        {
            // The following values are based on http://soundfile.sapp.org/doc/WaveFormat/
            var bitsPerSample = (ushort)16;
            var chunkID = "RIFF";
            var format = "WAVE";
            var subChunk1ID = "fmt ";
            var subChunk1Size = (uint)16;
            var audioFormat = (ushort)1;
            var numChannels = (ushort)clip.channels;
            var sampleRate = (uint)clip.frequency;
            var byteRate = (uint)(sampleRate * clip.channels * bitsPerSample / 8);  // SampleRate * NumChannels * BitsPerSample/8
            var blockAlign = (ushort)(numChannels * bitsPerSample / 8); // NumChannels * BitsPerSample/8
            var subChunk2ID = "data";
            var subChunk2Size = (uint)(data.Length * clip.channels * bitsPerSample / 8); // NumSamples * NumChannels * BitsPerSample/8
            var chunkSize = (uint)(36 + subChunk2Size); // 36 + SubChunk2Size
                                                        // Start writing the file.
            WriteString(stream, chunkID);
            WriteInteger(stream, chunkSize);
            WriteString(stream, format);
            WriteString(stream, subChunk1ID);
            WriteInteger(stream, subChunk1Size);
            WriteShort(stream, audioFormat);
            WriteShort(stream, numChannels);
            WriteInteger(stream, sampleRate);
            WriteInteger(stream, byteRate);
            WriteShort(stream, blockAlign);
            WriteShort(stream, bitsPerSample);
            WriteString(stream, subChunk2ID);
            WriteInteger(stream, subChunk2Size);
            foreach (var sample in data)
            {
                // De-normalize the samples to 16 bits.
                var deNormalizedSample = (short)0;
                if (sample > 0)
                {
                    var temp = sample * short.MaxValue;
                    if (temp > short.MaxValue)
                        temp = short.MaxValue;
                    deNormalizedSample = (short)temp;
                }
                if (sample < 0)
                {
                    var temp = sample * (-short.MinValue);
                    if (temp < short.MinValue)
                        temp = short.MinValue;
                    deNormalizedSample = (short)temp;
                }
                WriteShort(stream, (ushort)deNormalizedSample);
            }
        }
        void WriteString(Stream stream, string value)
        {
            foreach (var character in value)
                stream.WriteByte((byte)character);
        }

        void WriteInteger(Stream stream, uint value)
        {
            stream.WriteByte((byte)(value & 0xFF));
            stream.WriteByte((byte)((value >> 8) & 0xFF));
            stream.WriteByte((byte)((value >> 16) & 0xFF));
            stream.WriteByte((byte)((value >> 24) & 0xFF));
        }

        void WriteShort(Stream stream, ushort value)
        {
            stream.WriteByte((byte)(value & 0xFF));
            stream.WriteByte((byte)((value >> 8) & 0xFF));
        }
    }
}
