using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Pieciolinia : MonoBehaviour
{
    public List<Tact> leftHandTrack = new List<Tact>();
    public List<Tact> rightHandTrack = new List<Tact>();
    [SerializeField] GameObject leftHandMusicalKey;
    [SerializeField] GameObject rightHandMusicalKey;
    [SerializeField] Image ArrowLeft;
    [SerializeField] Image ArrowRight;
    [SerializeField] GameObject leftHandTact;
    [SerializeField] GameObject rightHandTact;
    [SerializeField] ProjectContoller projectContoller;
    [SerializeField] Piano piano;
    Vector3 tactMovement = new Vector3(305, 0, 0);
    Vector3 moveTact = new Vector3(305, 0, 0);
    Vector3 soundMovement = new Vector3(2.824074f, 0, 0);
    int tactMoves = 0;
    int arrowRightMovementStart = 1;

    public void AddTacts(Tact leftHandTact, Tact rightHandTact)
    {
        if (leftHandTact != null) { leftHandTrack.Add(leftHandTact); }
        if (rightHandTact != null) { rightHandTrack.Add(rightHandTact); }
    }
    public void OnButtonArrowLeftClicked()
    {
        if (tactMoves > 0)
        {
            ArrowRight.color = Color.black;
            MoveTact(false);
            if (tactMoves == 0)
            {
                ArrowLeft.color = Color.gray;
            }
        }
        else
        {
            ArrowLeft.color = Color.gray;
        }
    }
    public void UpdateArrowRight()
    {
        if (tactMoves + arrowRightMovementStart <= leftHandTrack.Count - 1)
        {
            ArrowRight.color = Color.black;
        }
        else
        {
            ArrowRight.color = Color.gray;
        }
    }
    public void OnButtonArrowRightClicked()
    {
        if (tactMoves + arrowRightMovementStart <= leftHandTrack.Count - 1)
        {
            ArrowLeft.color = Color.black;
            MoveTact(true);
            if (tactMoves == leftHandTrack.Count - 1)
            {
                ArrowRight.color = Color.gray;
            }
        }
        else
        {
            ArrowRight.color = Color.gray;
        }
    }
    public void MoveTact(bool state)
    {
        int i = 1;
        if (state) { i = -1; }
        foreach (Tact tact in leftHandTrack)
        {
            tact.gameObject.transform.localPosition += tactMovement * i;
            foreach (Sound sound in tact.sounds)
            {
                sound.gameObject.transform.position += soundMovement * i;
            }
        }
        foreach (Tact tact in rightHandTrack)
        {
            tact.gameObject.transform.localPosition += tactMovement * i;
            foreach (Sound sound in tact.sounds)
            {
                sound.gameObject.transform.position += soundMovement * i;
            }
        }
        rightHandMusicalKey.transform.localPosition += tactMovement * i;
        leftHandMusicalKey.transform.localPosition += tactMovement * i;
        tactMoves -= i;
    }
    public void AddTact()
    {
        GameObject gameObject = Instantiate(leftHandTact, transform);
        gameObject.transform.localPosition = leftHandTrack.Last().gameObject.transform.localPosition + moveTact;
        leftHandTrack.Add(gameObject.GetComponent<Tact>());
        projectContoller.tacts++;
        leftHandTrack.Last().ClearSounds();
        leftHandTrack.Last().Determine_TactNumer_hand(projectContoller.tacts, E_Hand.left);
        GameObject gameObject1 = Instantiate(rightHandTact, transform);
        gameObject1.transform.localPosition = rightHandTrack.Last().gameObject.transform.localPosition + moveTact;
        rightHandTrack.Add(gameObject1.GetComponent<Tact>());
        rightHandTrack.Last().ClearSounds();
        rightHandTrack.Last().Determine_TactNumer_hand(projectContoller.tacts, E_Hand.right);
        piano.GetComponent<Piano>().UpdateTactDropDown(projectContoller.tacts);
        piano.GetComponent<Piano>().UpdateDropdownPosition(false);
        if (projectContoller.tacts > 5 + tactMoves)
        {
            OnButtonArrowRightClicked();
        }
        UpdateArrowRight();
    }
    public void RemoveSound(Sound sound) 
    {
        Tact tact;
        if (sound.hand == E_Hand.left) { tact = leftHandTrack[sound.tact - 1]; }
        else { tact = rightHandTrack[sound.tact - 1]; }
        for (int i=0;i<tact.sounds.Count;i++) 
        {
            if (tact.sounds[i] == sound) 
            {
                tact.sounds.RemoveAt(i);
            }
        }
    }
}
