using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchDetector : MonoBehaviour
{
    [SerializeField] GameObject soundSettings;
    Sound lastClickedSound;
    private void Update()
    {
        OnMouseClick();
    }
    void OnMouseClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(mousePosition, Vector2.zero);
            if (hit.collider != null)
            {
                if (hit.collider.gameObject.GetComponent<Key>() != null)
                {
                    this.GetComponent<ProjectContoller>().piano.OnPianoKeyClicked(hit.collider.gameObject);
                }
                else if (hit.collider.gameObject.GetComponent<Sound>() != null)
                {
                    lastClickedSound?.SoundWasNotClicked();
                    soundSettings.SetActive(true);
                    lastClickedSound = hit.collider.gameObject.GetComponent<Sound>();
                    lastClickedSound.SoundWasClicked();
                    soundSettings.GetComponent<SoundSettings>().OnSoundClickedAction(lastClickedSound.gameObject);
                }
            }
        }
    }
}
