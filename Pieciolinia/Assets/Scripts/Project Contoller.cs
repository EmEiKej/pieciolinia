using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using UnityEngine.XR;
using Image = UnityEngine.UI.Image;

public enum E_Hand { left, right }
public enum E_SoundName
{
    F, E, D, C, B, A, G
}
public class ProjectContoller : MonoBehaviour
{
    [SerializeField] List<Sprite> notes;
    [SerializeField] public List<AudioClip> audioClips;
    [SerializeField] public GameObject sound;
    [SerializeField] public Tact leftHandTact;
    [SerializeField] public Tact rightHandTact;
    [SerializeField] Pieciolinia pieciolinia;
    [SerializeField] public Piano piano;
    public int positions = 16;
    public Dictionary<E_SoundName, float> leftHandnotePosition = new Dictionary<E_SoundName, float>();
    public Dictionary<E_SoundName, float> rightHandnotePosition = new Dictionary<E_SoundName, float>();
    public int tacts = 1;
    public string projectName;
    public string autorName;
    public string licence;
    public string filePath;
    public string fileType;
    public float activeNote = 2f;
    public bool isSilent = false;
    List<float> leftHandSilentNotesY = new List<float> { 2.75f, 2.68f, 2.624f, 2.608f, 2.479f };
    List<float> rightHandSilentNotesY = new List<float>();
    float positionDiference = -3.369f;


    private void Start()
    {
        InitializeTact();
        FillDictionary();
        FillListRigthHandSilentsNotesY();
    }
    void FillListRigthHandSilentsNotesY()
    {
        for (int i = 0; i < leftHandSilentNotesY.Count; i++)
        {
            rightHandSilentNotesY.Add(leftHandSilentNotesY[i] + positionDiference);
        }
    }
    public void ChangeNoteAndState(float note, bool isSilent)
    {
        activeNote = note;
        this.isSilent = isSilent;
    }
    public void DetermineProjectInfo(string projectName, string autorName, string licence, string filePath, string fileType)
    {
        this.projectName = projectName;
        this.autorName = autorName;
        this.licence = licence;
        this.filePath = filePath;
        this.fileType = fileType;
    }
    public bool InitSound(E_SoundName chosenSound, string chosenTone, int chosenPosition, int chosenTact, E_Hand hand)
    {
        float y = 0;
        bool wasUsed = false;
        float xPosition = pieciolinia.leftHandTrack[chosenTact - 1].gameObject.transform.GetChild(5).transform.GetChild(chosenPosition - 1).GetComponent<Transform>().position.x;
        if (hand == E_Hand.left && pieciolinia.leftHandTrack[chosenTact - 1].SoundCanBeAdded(chosenPosition - 1, activeNote))
        {
            GameObject gameObject = Instantiate(sound);
            if (isSilent)
            {
                gameObject.name = "Silence";
                gameObject.transform.GetChild(0).GetComponent<AudioSource>().clip = audioClips.Last();
                y = ChooseYPosition(hand);
            }
            else
            {
                gameObject.name = chosenSound.ToString() + chosenTone;
                for (int i = 0; i < audioClips.Count; i++)
                {
                    if (audioClips[i].name == chosenSound.ToString() + chosenTone)
                    {
                        gameObject.transform.GetChild(0).GetComponent<AudioSource>().clip = audioClips[i];
                        break;
                    }
                }
                y = leftHandnotePosition[chosenSound]+DetermineYPositionOfNote(activeNote);
            }
            gameObject.GetComponent<Transform>().position = new Vector2(xPosition, y);
            pieciolinia.leftHandTrack[chosenTact - 1].AddSound(gameObject.GetComponent<Sound>());
            pieciolinia.leftHandTrack[chosenTact - 1].sounds.Last().Determine_Sound_Tone_Position_Tact(chosenSound.ToString(), int.Parse(chosenTone), activeNote, chosenPosition, chosenTact, E_Hand.left);
            pieciolinia.leftHandTrack[chosenTact - 1].UpdateAvaiablePositions(chosenPosition - 1, activeNote, true);
            DetermineSoundImage(gameObject);
            wasUsed = true;
        }
        else if (hand == E_Hand.right && pieciolinia.rightHandTrack[chosenTact - 1].SoundCanBeAdded(chosenPosition - 1, activeNote))
        {
            GameObject gameObject = Instantiate(sound);
            if (isSilent)
            {
                gameObject.name = "Silence";
                gameObject.transform.GetChild(0).GetComponent<AudioSource>().clip = audioClips.Last();
                y = ChooseYPosition(hand);
            }
            else
            {
                gameObject.name = chosenSound.ToString() + chosenTone;
                for (int i = 0; i < audioClips.Count; i++)
                {
                    if (audioClips[i].name == chosenSound.ToString() + chosenTone)
                    {
                        gameObject.transform.GetChild(0).GetComponent<AudioSource>().clip = audioClips[i];
                        break;
                    }
                }
                y = rightHandnotePosition[chosenSound] + DetermineYPositionOfNote(activeNote);
            }
            gameObject.GetComponent<Transform>().position = new Vector2(xPosition, y);
            pieciolinia.rightHandTrack[chosenTact - 1].AddSound(gameObject.GetComponent<Sound>());
            pieciolinia.rightHandTrack[chosenTact - 1].UpdateAvaiablePositions(chosenPosition - 1, activeNote, true);
            pieciolinia.rightHandTrack[chosenTact - 1].sounds.Last().Determine_Sound_Tone_Position_Tact(chosenSound.ToString(), int.Parse(chosenTone), activeNote, chosenPosition, chosenTact, E_Hand.right);
            DetermineSoundImage(gameObject);
            wasUsed = true;
        }
        if (wasUsed) { return true; }
        else { return false; }
    }
    public void DelateSound(E_SoundName chosenSound, string chosenTone, int chosenPosition, int chosenTact, E_Hand hand)
    {
        if (hand == E_Hand.left)
        {
            for (int i = 0; i < pieciolinia.leftHandTrack[chosenTact - 1].sounds.Count; i++)
            {
                if (pieciolinia.leftHandTrack[chosenTact - 1].sounds[i].sound == chosenSound.ToString() && pieciolinia.leftHandTrack[chosenTact - 1].sounds[i].tone == int.Parse(chosenTone) && pieciolinia.leftHandTrack[chosenTact - 1].sounds[i].position == chosenPosition && pieciolinia.leftHandTrack[chosenTact - 1].sounds[i].tact == chosenTact)
                {
                    Destroy(pieciolinia.leftHandTrack[chosenTact - 1].sounds[i].gameObject);
                    pieciolinia.leftHandTrack[chosenTact - 1].sounds.RemoveAt(i);
                    break;
                }
            }
        }
        else if (hand == E_Hand.right)
        {
            for (int i = 0; i < pieciolinia.rightHandTrack[chosenTact - 1].sounds.Count; i++)
            {
                if (pieciolinia.rightHandTrack[chosenTact - 1].sounds[i].sound == chosenSound.ToString() && pieciolinia.rightHandTrack[chosenTact - 1].sounds[i].tone == int.Parse(chosenTone) && pieciolinia.rightHandTrack[chosenTact - 1].sounds[i].position == chosenPosition && pieciolinia.rightHandTrack[chosenTact - 1].sounds[i].tact == chosenTact)
                {
                    Destroy(pieciolinia.rightHandTrack[chosenTact - 1].sounds[i].gameObject);
                    pieciolinia.rightHandTrack[chosenTact - 1].sounds.RemoveAt(i);
                    break;
                }
            }
        }
    }
    public void FillDropdownSound(TMP_Dropdown dropdownSound)
    {
        foreach (E_SoundName sound in Enum.GetValues(typeof(E_SoundName)))
        {
            dropdownSound.options.Add(new TMP_Dropdown.OptionData(sound.ToString()));
        }
        dropdownSound.captionText.text = dropdownSound.options[dropdownSound.value].text;
    }
    public void FillDropdownTone(TMP_Dropdown dropdownTone, char sound)
    {
        bool wasIn = false;
        for (int i = 0; i < audioClips.Count; i++)
        {
            if (audioClips[i].name.First() == sound && (audioClips[i].name.Count() == 2))
            {
                wasIn = true;
                dropdownTone.options.Add(new TMP_Dropdown.OptionData(audioClips[i].name.Last().ToString()));
            }
            else if (wasIn) { break; }
        }
        dropdownTone.value = 1;
        dropdownTone.captionText.text = dropdownTone.options[dropdownTone.value].text;
    }
    public void FillDropdownPosition(TMP_Dropdown dropdownPosition)
    {
        for (int i = 1; i < positions + 1; i++)
        {
            dropdownPosition.options.Add(new TMP_Dropdown.OptionData(i.ToString()));
        }
        dropdownPosition.captionText.text = dropdownPosition.options[dropdownPosition.value].text;
    }
    public void FillDropdownTact(TMP_Dropdown dropdownTact)
    {
        dropdownTact.options.Add(new TMP_Dropdown.OptionData(tacts.ToString()));
        dropdownTact.captionText.text = dropdownTact.options[dropdownTact.value].text;
    }
    void FillDictionary()
    {
        List<float> y = new List<float>();
        float firstLinePosition_y = leftHandTact.gameObject.transform.GetChild(0).GetComponent<Transform>().position.y;
        float secondLinePosition_y = leftHandTact.gameObject.transform.GetChild(1).GetComponent<Transform>().position.y;
        for (int i = 0; i < 7; i++)
        {
            y.Add(firstLinePosition_y - (firstLinePosition_y - secondLinePosition_y) * i / 2);
        }
        for (int i = 0; i < 7; i++)
        {
            leftHandnotePosition.Add((E_SoundName)i, y[i]);
        }
        float diference = firstLinePosition_y - rightHandTact.gameObject.transform.GetChild(0).GetComponent<Transform>().position.y;
        for (int i = 0; i < 7; i++)
        {
            rightHandnotePosition.Add((E_SoundName)i, y[i] - diference);
        }
    }
    void InitializeTact()
    {
        pieciolinia.AddTacts(leftHandTact, rightHandTact);
        leftHandTact.Determine_TactNumer_hand(tacts, E_Hand.left);
        rightHandTact.Determine_TactNumer_hand(tacts, E_Hand.right);
    }

    void DetermineSoundImage(GameObject sound)
    {
        if (!isSilent)
        {
            if (activeNote == 4f)
            {
                sound.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = notes[0];
            }
            else if (activeNote == 2f)
            {
                sound.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = notes[1];
            }
            else if (activeNote == 1f)
            {
                sound.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = notes[2];
            }
            else if (activeNote == 0.5f)
            {
                sound.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = notes[3];
            }
            else if (activeNote == 0.25f)
            {
                sound.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = notes[4];
            }
        }
        else
        {
            if (activeNote == 4f)
            {
                sound.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = notes[5];
            }
            else if (activeNote == 2f)
            {
                sound.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = notes[6];
            }
            else if (activeNote == 1f)
            {
                sound.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = notes[7];
            }
            else if (activeNote == 0.5f)
            {
                sound.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = notes[8];
            }
            else if (activeNote == 0.25f)
            {
                sound.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = notes[9];
            }
        }

    }
    float ChooseYPosition(E_Hand hand)
    {
        List<float> YPositions = new List<float>();
        if (hand == E_Hand.left) { YPositions = leftHandSilentNotesY; }
        else { YPositions = rightHandSilentNotesY; }
        if (activeNote == 4f)
        {
            return YPositions[0];
        }
        else if (activeNote == 2f)
        {
            return YPositions[1];
        }
        else if (activeNote == 1f)
        {
            return YPositions[2];
        }
        else if (activeNote == 0.5f)
        {
            return YPositions[3];
        }
        else  //activeNote == 0.25f
        {
            return YPositions[4];
        }
    }
    public float DetermineYPositionOfNote(float activeNote)
    {
        if (activeNote == 2f || activeNote == 1f || activeNote == 0.5f || activeNote == 0.25f)
        {
            return 0.204756f;
        }
        else { return 0; }
    }
}
