using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using UnityEngine.XR;

public class Piano : MonoBehaviour
{
    [SerializeField] ProjectContoller projectContoller;
    [SerializeField] Pieciolinia pieciolinia;
    [SerializeField] SettingsBar settingsBar;
    [SerializeField] GameObject pianoSetUp;
    [SerializeField] GameObject keysFolder;
    [SerializeField] List<GameObject> keys;
    [SerializeField] UnityEngine.UI.Image leftArrow;
    [SerializeField] UnityEngine.UI.Image rightArrow;
    [SerializeField] UnityEngine.UI.Image smartPiano;
    [SerializeField] TMP_Dropdown position;
    [SerializeField] TMP_Dropdown tact;
    [SerializeField] TMP_Dropdown hand;
    [SerializeField] List<UnityEngine.UI.Image> buttonImages;
    int firstActiveKey = 17;
    int lastActiveKey = 33;
    E_Hand currentHand = E_Hand.left;
    bool ShorterTactIsFull = false;
    UnityEngine.UI.Image lastClickedNote;
    private void Start()
    {
        projectContoller.FillDropdownPosition(position);
        projectContoller.FillDropdownTact(tact);
        DetermineKeys();
    }
    public void OnDropDownHandValueChanged()
    {
        currentHand = (E_Hand)Enum.Parse(typeof(E_Hand), hand.captionText.text);
        UpdateDropdownPosition(false);
    }
    public void OnButtonExitClicked()
    {
        this.gameObject.SetActive(false);
    }
    public void OnButtonPianoSetUpClicked()
    {
        pianoSetUp.SetActive(true);
    }
    void NoteButtonAction(int buttonNumer) 
    {
        if (lastClickedNote != null) { lastClickedNote.color = Color.black; }
        else { lastClickedNote = buttonImages[1];
            lastClickedNote.color = Color.black;
        }
        lastClickedNote = buttonImages[buttonNumer];
        buttonImages[buttonNumer].color = Color.yellow;
    }
    public void OnButtonWholeNoteClicked()
    {
        projectContoller.ChangeNoteAndState(4f, false);
        NoteButtonAction(0);
    }
    public void OnButtonHalfNoteClicked()
    {
        projectContoller.ChangeNoteAndState(2f, false);
        NoteButtonAction(1);
    }
    public void OnButtonQuarterNoteClicked()
    {
        projectContoller.ChangeNoteAndState(1f, false);
        NoteButtonAction(2);
    }
    public void OnButtonEighthNoteClicked()
    {
        projectContoller.ChangeNoteAndState(0.5f, false);
        NoteButtonAction(3);
    }
    public void OnButtonSixteenthNoteClicked()
    {
        projectContoller.ChangeNoteAndState(0.25f, false);
        NoteButtonAction(4);
    }
    public void OnButtonWholeNoteSkipClicked()
    {
        projectContoller.ChangeNoteAndState(4f, true);
        NoteButtonAction(5);
    }
    public void OnButtonHalfNoteSkipClicked()
    {
        projectContoller.ChangeNoteAndState(2f, true);
        NoteButtonAction(6);
    }
    public void OnButtonQuarterNoteSkipClicked()
    {
        projectContoller.ChangeNoteAndState(1f, true);
        NoteButtonAction(7);
    }
    public void OnButtonEighthNoteSkipClicked()
    {
        projectContoller.ChangeNoteAndState(0.5f, true);
        NoteButtonAction(8);
    }
    public void OnButtonSixteenthNoteSkipClicked()
    {
        projectContoller.ChangeNoteAndState(0.25f, true);
        NoteButtonAction(9);
    }
    public void OnButtonArrowLeftClicked()
    {
        if (firstActiveKey != 0)
        {
            MoveKeys(true);
            if (rightArrow.color == Color.gray)
            {
                rightArrow.color = Color.white;
            }
        }
        else
        {
            leftArrow.color = Color.gray;
        }
    }
    public void OnButtonArrowRightClicked()
    {
        if (lastActiveKey != keys.Count - 1)
        {
            MoveKeys(false);
            if (leftArrow.color == Color.gray)
            {
                leftArrow.color = Color.white;
            }
        }
        else
        {
            rightArrow.color = Color.gray;
        }
    }
    public void OnPianoSettiongsButtonExitClicked()
    {
        pianoSetUp.SetActive(false);
    }
    void MoveKeys(bool state)
    {
        //pianino sie zapetla
        int i = 1;
        int j = 1;
        if (!state) { i = -1; j = 0; }
        keysFolder.transform.localPosition += new Vector3(70 * i, 0, 0);
        keys[firstActiveKey - j].SetActive(state);
        keys[lastActiveKey + 1 - j].SetActive(!state);
        firstActiveKey -= i;
        lastActiveKey -= i;
    }
    public void OnPianoKeyClicked(GameObject pianoKey)
    {
        pianoKey.GetComponent<SpriteRenderer>().color = Color.gray;
        bool useFunction = projectContoller.InitSound(pianoKey.GetComponent<Key>().sound, pianoKey.GetComponent<Key>().tone.ToString(), int.Parse(position.captionText.text), int.Parse(tact.captionText.text), currentHand);
        if (useFunction)
        {
            UpdateDropdownPosition(true);
        }
        StartCoroutine(ChangeColor());
        IEnumerator ChangeColor()
        {
            yield return new WaitForSeconds(0.2f);
            pianoKey.GetComponent<SpriteRenderer>().color = Color.white;
        }
    }
    public void DetermineKeys()
    {
        for (int i = 0; i < keys.Count; i++)
        {
            keys[i].GetComponent<Key>().Determine_Sound_Tone((E_SoundName)Enum.Parse(typeof(E_SoundName), keys[i].name.First().ToString()), int.Parse(keys[i].name.Last().ToString()));
        }
    }
    public void UpdateTactDropDown(int value)
    {
        tact.options.Add(new TMP_Dropdown.OptionData(value.ToString()));
        tact.value++;
    }
    public void RemoveLastTactDropDown()
    {
        tact.options.RemoveAt(tact.options.Count - 1);
    }
    public void UpdateDropdownPosition(bool addTact)
    {
        Tact tact;
        if (currentHand == E_Hand.left) { tact = pieciolinia.leftHandTrack[this.tact.value]; }
        else { tact = pieciolinia.rightHandTrack[this.tact.value]; }
        position.value = 0;
        position.options.Clear();
        for (int i = 0; i < tact.avaiablePositions.Count; i++)
        {
            if (tact.avaiablePositions[i] == true)
            {
                position.options.Add(new TMP_Dropdown.OptionData((i + 1).ToString()));
            }
        }
        if (position.options.Count != 0) { position.captionText.text = position.options[position.value].text; }
        else
        {
            if (addTact && this.tact.value == this.tact.options.Count - 1)
            {
                pieciolinia.AddTact();
            }
            else
            {
                if (addTact && this.tact.value != this.tact.options.Count - 1)
                {
                    this.tact.value++;
                    UpdateDropdownPosition(false);
                }
                position.options.Add(new TMP_Dropdown.OptionData(("Empty").ToString()));
                position.captionText.text = position.options[0].text;

            }
        }
    }
    public void OnTactDropdownValueChanged()
    {
        UpdateDropdownPosition(false);
    }
}
