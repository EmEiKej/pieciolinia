using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SoundSettings : MonoBehaviour
{
    [SerializeField] Pieciolinia pieciolinia;
    [SerializeField] ProjectContoller projectContoller;
    [SerializeField] TMP_Dropdown sound;
    [SerializeField] TMP_Dropdown tone;
    GameObject clickedSound;
    private void Start()
    {
        projectContoller.FillDropdownSound(sound);
        projectContoller.FillDropdownTone(tone, 'A');
    }
    public void OnButtonDelateClicked()
    {
        pieciolinia.RemoveSound(clickedSound.GetComponent<Sound>());
        Destroy(clickedSound);
        this.gameObject.SetActive(false);
        //zr�b miejsce
    }
    public void OnSoundClickedAction(GameObject clickedSound)
    {
        this.clickedSound = clickedSound;
        for (int i = 0; i < sound.options.Count; i++)
        {
            if (sound.options[i].text == clickedSound.GetComponent<Sound>().sound)
            {
                sound.value = i;
                //sound.captionText.text = sound.options[sound.value].text;
            }
        }
        for (int i = 0; i < tone.options.Count; i++)
        {
            if (int.Parse(tone.options[i].text) == clickedSound.GetComponent<Sound>().tone)
            {
                tone.value = i;
                //tone.captionText.text = tone.options[tone.value].text;
            }
        }
    }
    public void OnButtonExitClicked()
    {
        clickedSound.GetComponent<Sound>().SoundWasNotClicked();
        this.gameObject.SetActive(false);
    }
    public void OnButtonChangeClicked()
    {
        clickedSound.GetComponent<Sound>().sound = sound.options[sound.value].text;
        clickedSound.GetComponent<Sound>().tone = int.Parse(tone.options[tone.value].text);
        if (clickedSound.GetComponent<Sound>().hand == E_Hand.left)
        {
            clickedSound.transform.position = new Vector3(clickedSound.transform.position.x, projectContoller.leftHandnotePosition[(E_SoundName)Enum.Parse(typeof(E_SoundName), clickedSound.GetComponent<Sound>().sound)] + projectContoller.DetermineYPositionOfNote(clickedSound.GetComponent<Sound>().note));
        }
        else
        {
            clickedSound.transform.position = new Vector3(clickedSound.transform.position.x, projectContoller.rightHandnotePosition[(E_SoundName)Enum.Parse(typeof(E_SoundName), clickedSound.GetComponent<Sound>().sound)] + projectContoller.DetermineYPositionOfNote(clickedSound.GetComponent<Sound>().note));
        }
    }
}
