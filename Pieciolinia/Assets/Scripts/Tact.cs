using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Tact : MonoBehaviour
{
    [SerializeField] ProjectContoller projectContoller;
    [SerializeField] TMP_Text tactNumerText;
    public List<Sound> sounds;
    public int tactNumer;
    E_Hand hand;
    public List<bool> avaiablePositions = new List<bool>();
    private void Awake()
    {
        InitListAvaiablePositions();
        
    }
    private void Start()
    {
        ShowDeckNumer();
    }
    void ShowDeckNumer() 
    {
        tactNumerText.text = projectContoller.tacts.ToString();
    } 
    public void AddSound(Sound sound)
    {
        sounds.Add(sound);
    }
    public void Determine_TactNumer_hand(int tactNumer, E_Hand hand)
    {
        this.tactNumer = tactNumer;
        this.hand = hand;
    }
    public void ClearSounds()
    {
        sounds.Clear();
    }
    void InitListAvaiablePositions()
    {
        if (avaiablePositions.Count != 0) { avaiablePositions.Clear(); }
        for (int i = 0; i < projectContoller.positions; i++)
        {
            avaiablePositions.Add(true);
        }
    }
    public void UpdateAvaiablePositions(int startPosition, float value, bool addingSound)
    {
        int endPosition = (int)(startPosition + value * 4);
        if (addingSound)
        {
            for (int i = startPosition; i < endPosition; i++)
            {
                avaiablePositions[i] = false;
            }
        }
    }
    public bool SoundCanBeAdded(int startPosition, float value) 
    {
        int endPosition = startPosition + (int)(value * 4)-1;
        if (endPosition >= projectContoller.positions) { return false; }
        for (int i = startPosition; i <= endPosition; i++)
        {
            if (avaiablePositions[i] == false) { return false; }
        }
        return true; 
    }
}
