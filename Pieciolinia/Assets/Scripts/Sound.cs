using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Sound : MonoBehaviour
{
    [SerializeField] public SpriteRenderer myRenderer;
    public string sound;
    public int tone;
    public float note;
    public int position;
    public int tact;
    public E_Hand hand;
    public void Determine_Sound_Tone_Position_Tact(string sound,int tone,float note, int position, int tact,E_Hand hand)
    {
        this.sound = sound;
        this.tone = tone;
        this.note = note;
        this.position = position;
        this.tact = tact;
        this.hand = hand;
    }
    public void SoundWasClicked() 
    {
        myRenderer.color = Color.yellow;
    }
    public void SoundWasNotClicked()
    {
        myRenderer.color = Color.black;
    }
}
