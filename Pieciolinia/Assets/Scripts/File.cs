using System.IO;
using TMPro;
using UnityEngine;
using UnityEditor;

public class File : MonoBehaviour
{
    [SerializeField] ProjectContoller projectContoller;
    [SerializeField] GameObject newProject;
    [SerializeField] GameObject openProject;
    [SerializeField] GameObject pieciolinia;
    [SerializeField] TMP_InputField autor;
    [SerializeField] TMP_InputField projectName;
    [SerializeField] TMP_InputField newPath;
    [SerializeField] TMP_InputField existingPath;
    [SerializeField] TMP_Dropdown licence;
    [SerializeField] TMP_Dropdown fileType;
    public void OnButton_File_Clicked()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }
    public void OnButton_NewProject_Clicked()
    {
        newProject.SetActive(true);
        gameObject.SetActive(false);
    }
#if UNITY_EDITOR
    [ContextMenu("Open Folder Explorer")]
    public void On_NewProject_Button_ChoosePath_Clicked()
    {
        string path = EditorUtility.OpenFolderPanel("Select a folder", "", "");
        if (!string.IsNullOrEmpty(path))
        {
            newPath.text = path;
        }
    }
#endif
#if UNITY_EDITOR
    [ContextMenu("Open Folder Explorer")]
    public void On_OpenProject_Button_ChoosePath_Clicked()
    {
        string path = EditorUtility.OpenFilePanel("Select a folder", "", fileType.captionText.text);
        if (!string.IsNullOrEmpty(path))
        {
            existingPath.text = path;
        }
    }
#endif
    public void OnButton_OpenProject_Clicked()
    {
        openProject.SetActive(true);
        gameObject.SetActive(false);
    }
    public void OnButton_Create_Clicked()
    {
        string autorName = autor.text;
        string projectNameText = projectName.text;
        string path = newPath.text;
        if (autorName == "" || projectNameText == "") { return; }
        if (!Directory.Exists(path)) { return; }
        newProject.SetActive(false);
        pieciolinia.SetActive(true);
        projectContoller.DetermineProjectInfo(projectNameText, autorName, licence.captionText.text, path, "");
    }
    public void On_NewProject_Button_Exit_Clicked()
    {
        newProject.SetActive(false);
    }
    public void On_OpenProject_Button_Exit_Clicked()
    {
        openProject.SetActive(false);
    }
}
